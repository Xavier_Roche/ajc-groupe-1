#ifndef JOUEUR_H
#define JOUEUR_H
#include <string>

class Joueur
{
    public:
        Joueur();
        virtual ~Joueur();
        void setNomPersonnage(string);
        string getNomPersonnage();

    protected:

    private:
        string nom;
        int slotDePersonnage;
        Personnage * Nouveau_Perso;
};

#endif // JOUEUR_H
